  
#show footnote.entry: set par(justify: true)

#set document(
  title: "Zur Elektrodynamik bewegter Körper",
  author: "Albert Einstein",
  keywords: "Physics",
  date: datetime(year: 1905, month:1, day: 1)
)

#set text(
  font: "Libre Bodoni",
  size: 12pt,
  lang: "de"
)

#set page(
  paper: "a5",
  margin: (x: 2.2cm, y: 2.2cm, top: 2.2cm, bottom: 2.2cm),
  number-align: right,
)

#show par: set block(spacing: 0.65em)

#show "Newton": name => box[ #set text(tracking: 1.2pt); #name ]
#show "Maxwell": name => box[ #set text(tracking: 1.2pt); #name ]
#show "Hertz": name => box[ #set text(tracking: 1.2pt); #name ]

#show heading.where(level:1): rm => [
  #h(8pt)
  #set align(center)
  #set text(size: 11pt, tracking: 1.2pt)
  #par(rm.body)
]

#set page(header: locate(loc => {
  if counter(page).at(loc).first() == 1 [
    #h(1fr)
    #counter(page).display(
      "891"
    )
] else if calc.odd(counter(page).at(loc).first()) [
  #set align(center)
  #set text(size: 11pt)
   #h(1fr)
  _Zur Elektrodynamik bewegter Körper_
   #h(1fr)
   #counter(page).display()
] else [
  #set align(center)
  #set text(size: 11pt)
   #counter(page).display()
   #h(1fr)
  _A. Einstein_
   #h(1fr)
]
}))

#counter(page).update(n => n + 890)

#show heading.where(level:2): rm => [
  #h(8pt)
  #set align(center)
  #set text(size: 11pt)
  #par(rm.body)
  #h(8pt)
]

#set par(
  justify: true,
  leading: 0.52em,
  first-line-indent: 2.5em,
)

#let title = [
3. Zur Elektrodynamik bewegter Körper;

von A. Einstein.
]

#align(center, text(font: "Bodoni Moda", 12pt )[
  #title
  #line(length: 10%)
])


Daß die Elektrodynamik Maxwells — wie dieselbe gegenwärtig aufgefaßt zu werden
pflegt — in ihrer Anwendung auf bewegte Körper zu Asymmetrien führt, welche den
Phänomenen nicht anzuhaften scheinen, ist bekannt. Man denke z. B. an die
elektrodynamische Wechselwirkung zwischen einem Magneten und einem Leiter. Das
beobachtbare Phänomen hängt hier nur ab von der Relativbewegung von Leiter und
Magnet, während nach der üblichen Auffassung die beiden Fälle, daß der eine
oder der andere dieser Körper der bewegte sei, streng voneinander zu trennen
sind. Bewegt sich nämlich der Magnet und ruht der Leiter, so entsteht in der
Umgebung des Magneten ein elektrisches Feld von gewissem Energiewerte, welches
an den Orten, wo sich Teile des Leiters befinden, einen Strom erzeugt. Ruht
aber der Magnet und bewegt sich der Leiter, so entsteht in der Umgebung des
Magneten kein elektrisches Feld, dagegen im Leiter eine elektromotorische
Kraft, welcher an sich keine Energie entspricht, die aber — Gleichheit der
Relativbewegung bei den beiden ins Auge gefaßten Fällen vorausgesetzt — zu
elektrischen Strömen von derselben Größe und demselben Verlaufe Veranlassung
gibt, wie im ersten Falle die elektrischen Kräfte.

Beispiele ähnlicher Art, sowie die mißlungenen Versuche, eine Bewegung der Erde
relativ zum „Lichtmedium“ zu konstatieren, führen zu der Vermutung, daß dem
Begriffe der absoluten Ruhe nicht nur in der Mechanik, sondern auch in der
Elektrodynamik keine Eigenschaften der Erscheinungen entsprechen, sondern daß
vielmehr für alle Koordinatensysteme, für welche die mechanischen Gleichungen
gelten, auch die gleichen elektrodynamischen und optischen Gesetze gelten, wie
dies für die Größen erster Ordnung bereits erwiesen ist. Wir wollen diese
Vermutung (deren Inhalt im folgenden „Prinzip der Relativität“ genannt werden
wird) zur Voraussetzung erheben und außerdem die mit ihm nur scheinbar
unverträgliche Voraussetzung einführen, daß sich das Licht im leeren Raume
stets mit einer bestimmten, vom Bewegungszustände des emittierenden Körpers
unabhängigen Geschwindigkeit $V$ fortpflanze.  Diese beiden Voraussetzungen
genügen, um zu einer einfachen und widerspruchsfreien Elektrodynamik bewegter
Körper zu gelangen unter Zugrundelegung der Maxwell sehen Theorie für ruhende
Körper. Die Einführung eines „Lichtäthers“ wird sich insofern als überflüssig
erweisen, als nach der zu entwickelnden Auffassung weder ein mit besonderen
Eigenschaften ausgestatteter „absolut ruhender Raum“ eingeführt, noch einem
Punkte des leeren Raumes, in welchem elektromagnetische Prozesse stattfinden,
ein Geschwindigkeitsvektor zugeordnet wird.

Die zu entwickelnde Theorie stützt sich — wie jede andere Elektrodynamik — auf
die Kinematik des starren Körpers, da die Aussagen einer jeden Theorie
Beziehungen zwischen starren Körpern (Koordinatensystemen), Uhren und
elektromagnetischen Prozessen betreffen. Die nicht genügende Berücksichtigung
dieses Umstandes ist die Wurzel der Schwierigkeiten, mit denen die
Elektrodynamik bewegter Körper gegenwärtig zu kämpfen hat.

= I. Kinematischer Teil.
== § 1. Definition der Gleichzeitigkeit.

Es liege ein Koordinatensystem vor, in welchem die
Newtonschen mechanischen Gleichungen gelten. Wir nennen
dies Koordinatensystem zur sprachlichen Unterscheidung von
später einzuführenden Koordinatensystemen und zur Präzisierung 
der Vorstellung das „ruhende System“.

Ruht ein materieller Punkt relativ zu diesem Koordinatensystem , so kann seine
Lage relativ zu letzterem durch starre Maßstäbe unter Benutzung der Methoden
der euklidischen Geometrie bestimmt und in kartesischen Koordinaten ausgedrückt
werden.

Wollen wir die _Bewegung_ eines materiellen Punktes beschreiben, so geben wir
die Werte seiner Koordinaten in Funktion der Zeit. Es ist nun wohl im Auge zu
behalten, daß eine derartige mathematische Beschreibung erst dann einen
physikalischen Sinn hat, wenn man sich vorher darüber klar geworden ist, was
hier unter „Zeit“ verstanden wird.  Wir haben zu berücksichtigen, daß alle
unsere Urteile, in welchen die Zeit eine Rolle spielt, immer Urteile über
_gleichzeitige Ereignisse_ sind. Wenn ich z. B. sage: „Jener Zug kommt hier um
7 Uhr an,“ so heißt dies etwa: „Das Zeigen des kleinen Zeigers meiner Uhr auf 7
und das Ankommen des Zuges sind gleichzeitige Ereignisse.“ #footnote[Die
Ungenauigkeit, welche in dem Begriffe der Gleichzeitigkeit zweier Ereignisse an
(annähernd) demselben Orte steckt und gleichfalls durch eine Abstraktion
überbrückt werden muß, soll hier nicht erörtert werden.]

Es könnte scheinen, daß alle die Definition der „Zeit“ betreffenden
Schwierigkeiten dadurch überwunden werden könnten, daß ich an Stelle der „Zeit“
die „Stellung des kleinen Zeigers meiner Uhr“ setze. Eine solche Definition
genügt in der Tat, wenn es sich darum handelt, eine Zeit zu definieren
ausschließlich für den Ort, an welchem sich die Uhr eben befindet; die
Definition genügt aber nicht mehr, sobald es sich darum handelt, an
verschiedenen Orten stattfindende Ereignisreihen miteinander zeitlich zu
verknüpfen, oder — was auf dasselbe hinausläuft — Ereignisse zeitlich zu
werten, welche in von der Uhr entfernten Orten stattfinden.

Wir könnten uns allerdings damit begnügen, die Ereignisse dadurch zeitlich zu
werten, daß ein samt der Uhr im Koordinatenursprung befindlicher Beobachter
jedem von einem zu wertenden Ereignis Zeugnis gebenden, durch den leeren Baum
zu ihm gelangenden Lichtzeichen die entsprechende Uhrzeigerstellung zuordnet.
Eine solche Zuordnung bringt aber den Übelstand mit sich, daß sie vom
Standpunkte des mit der Uhr versehenen Beobachters nicht unabhängig ist, wie
wir durch die Erfahrung wissen. Zu einer weit praktischeren Festsetzung
gelangen wir durch folgende Betrachtung.  Befindet sich im Punkte $A$ des
Baumes eine Uhr, so kann ein in $A$ befindlicher Beobachter die Ereignisse in
der unmittelbaren Umgebung von $A$ zeitlich werten durch Aufsuchen der mit
diesen Ereignissen gleichzeitigen Uhrzeigerstellungen.  Befindet sich auch im
Punkte $B$ des Baumes eine Uhr — wir wollen hinzufügen, „eine Uhr von genau
derselben Beschaffenheit wie die in $A$ befindliche“ — so ist auch eine
zeitliche Wertung der Ereignisse in der unmittelbaren Umgebung von $B$ durch
einen in $B$ befindlichen Beobachter möglich. Es ist aber ohne weitere
Festsetzung nicht möglich, ein Ereignis in $A$ mit einem Ereignis in $B$
zeitlich zu vergleichen; wir haben bisher nur eine „A-Zeit“ und eine „B-Zeit“,
aber keine für $A$ und $B$ gemeinsame „Zeit“ definiert. Die letztere Zeit kann
nun definiert werden, indem man durch Definition festsetzt, daß die „Zeit“,
welche das Licht braucht, um von $A$ nach $B$ zu gelangen, gleich ist der
„Zeit“, welche es braucht, um von $B$ nach $A$ zu gelangen. Es gehe nämlich ein
Lichtstrahl zur „$A$-Zeit“ $t_A$ von $A$ nach $B$ ab, werde zur „$B$-Zeit“ $t_B$ in
$B$ gegen $A$ zu reflektiert und gelange zur „$A$-Zeit“ $t'_A$ nach $A$ zurück. Die
beiden Uhren laufen definitionsgemäß synchron, wenn 

$ t_B - t_A = t'_A — t_B. $

Wir nehmen an, daß diese Definition des Synchronismus
in widerspruchsfreier Weise möglich sei, und zwar für beliebig
viele Punkte, daß also allgemein die Beziehungen gelten:

1. Wenn die Uhr in $B$ synchron mit der Uhr in $A$ läuft, so läuft die Uhr in $A$ synchron mit der Uhr in $B$.

2. Wenn die Uhr in $A$ sowohl mit der Uhr in $B$ als auch mit der Uhr in $C$ synchron läuft, so laufen auch die Uhren in $B$ und $C$ synchron relativ zueinander.

Wir haben so unter Zuhilfenahme gewisser (gedachter)
physikalischer Erfahrungen festgelegt, was unter synchron
laufenden, an verschiedenen Orten befindlichen, ruhenden
Uhren zu verstehen ist und damit offenbar eine Definition
von „gleichzeitig“ und „Zeit“ gewonnen. Die „Zeit“ eines
Ereignisses ist die mit dem Ereignis gleichzeitige Angabe
einer am Orte des Ereignisses befindlichen, ruhenden Uhr,
welche mit einer bestimmten, ruhenden Uhr, und zwar für
alle Zeitbestimmungen mit dex nämlichen Uhr, synchron läuft.
Wir setzen noch der Erfahrung gemäß fest, daß die
Größe

$ frac(2 overline(A B), t'_A - t_A) = V $

eine universelle Konstante (die Lichtgeschwindigkeit im leeren
Baume) sei.

Wesentlich ist, daß wir die Zeit mittels im ruhenden System
ruhender Uhren definiert haben; wir nennen die eben definierte
Zeit wegen dieser Zugehörigkeit zum ruhenden System „die
Zeit des ruhenden Systems“.

== § 2. Über die Relativität von Längen und Zeiten.

Die folgenden Überlegungen stützen sich auf das Relativitätsprinzip und auf das
Prinzip der Konstanz der Lichtgeschwindigkeit, welche beiden Prinzipien wir
folgendermaßen definieren.

$1$. Die Gesetze, nach denen sich die Zustände der physikalischen Systeme ändern, sind unabhängig davon, auf welches von zwei relativ zueinander in gleichförmiger Translationsbewegung befindlichen Koordinatensystemen diese Zustandsänderungen bezogen werden.

$2$. Jeder Lichtstrahl bewegt sich im „ruhenden“ Koordinatensystem mit der
bestimmten Geschwindigkeit V, unabhängig davon, ob dieser Lichtstrahl von einem
ruhenden oder bewegten Körper emittiert ist. Hierbei ist $ "Geschwindigkeit" =
frac("Lichtweg", "Zeitdauer") , $  wobei „Zeitdauer“ im Sinne der Definition
des § 1 aufzufassen ist.

Es sei ein ruhender starrer Stab gegeben; derselbe besitze, mit einem ebenfalls
ruhenden Maßstabe gemessen, die Länge l. Wir denken uns nun die Stabachse in
die $X$-Achse des ruhenden Koordinatensystems gelegt und dem Stabe hierauf eine
gleichförmige Paralleltranslationsbewegung (Geschwindigkeit $v$) längs der
$X$-Achse im Sinne der wachsenden $x$ erteilt.  Wir fragen nun nach der Länge des
bewegten Stabes, welche wir uns durch folgende zwei Operationen ermittelt
denken:

a) Der Beobachter bewegt sich samt dem vorher genannten Maßstabe mit dem
auszumessenden Stabe und mißt direkt durch Anlegen des Maßstabes die Länge des
Stabes, ebenso, wie wenn sich auszumessender Stab, Beobachter und Maßstab in
Ruhe befänden.

b) Der Beobachter ermittelt mittels im ruhenden Systeme aufgestellter, gemäß §
1 synchroner, ruhender Uhren, in welchen Punkten des ruhenden Systems sich
Anfang und Ende des auszumessenden Stabes zu einer bestimmten Zeit $t$ befinden.

Die Entfernung dieser beiden Punkte, gemessen mit dem schon benutzten, in
diesem Falle ruhenden Maßstabe ist ebenfalls eine Länge, welche man als „Länge
des Stabes“ bezeichnen kann.

Nach dem Relativitätsprinzip muß die bei der Operation a) zu findende Länge,
welche wir „die Länge des Stabes im bewegten System“ nennen wollen, gleich der
Länge $l$ des ruhenden Stabes sein.

Die bei der Operation b) zu findende Länge, welche wir „die Länge des
(bewegten) Stabes im ruhenden System“ nennen wollen, werden wir unter
Zugrundelegung unserer beiden Prinzipien bestimmen und finden, daß sie von $l$
verschieden ist.

Die allgemein gebrauchte Kinematik nimmt stillschweigend an, daß die durch die
beiden erwähnten Operationen bestimmten Längen einander genau gleich seien,
oder mit anderen Worten, daß ein bewegter starrer Körper in der Zeitepoche $t$ in
geometrischer Beziehung vollständig durch _denselben_ Körper, wenn er in
bestimmter Lage _ruht_, ersetzbar sei.

Wir denken uns ferner an den beiden Stabenden ($A$ und $B$)
Uhren angebracht, welche mit den Uhren des ruhenden Systems
synchron sind, d. h. deren Angaben jeweilen der „Zeit des
ruhenden Systems“ an den Orten, an welchen sie sich gerade
befinden, entsprechen; diese Uhren sind also „synchron im
ruhenden System“.

Wir denken uns ferner, daß sich bei jeder Uhr ein mit
ihr bewegter Beobachter befinde, und daß diese Beobachter
auf die beiden Uhren das im § 1 aufgestellte Kriterium für
den synchronen Gang zweier Uhren anwenden. Zur Zeit
#footnote[„Zeit“ bedeutet hier „Zeit, des ruhenden Systems“ und zugleich
„Zeigerstellung der bewegten Uhr, welche sieh an dem Orte, von dem
die Rede ist, befindet“.]

$t_A$ gehe ein Lichtstrahl von $A$ aus, werde zur Zeit $t_B$ in $B$
reflektiert und gelange zur Zeit $t'_A$ nach $A$ zurück. 
Unter Berücksichtigung des Prinzipes von der Konstanz der Lichtgeschwindigkeit finden wir:

$ t_B - t_A = frac(r_(A B), V-v) $

und

$ t'_A - t_B = frac(r_(A B), V+v), $

wobei $r_(A B)$ die Länge des bewegten Stabes — im ruhenden System gemessen —
bedeutet. Mit dem bewegten Stabe bewegte Beobachter würden also die beiden
Uhren nicht synchron gehend finden, während im ruhenden System befindliche
Beobachter die Uhren als synchron laufend erklären würden.

Wir sehen also, daß wir dem Begriffe der Gleichzeitigkeit keine absolute
Bedeutung beimessen dürfen, sondern daß zwei Ereignisse, welche, von einem
Koordinatensystem aus betrachtet, gleichzeitig sind, von einem relativ zu
diesem System bewegten System aus betrachtet, nicht mehr als gleichzeitige
Ereignisse aufzufassen sind.

== § 3. Theorie der Koordinaten- und Zeittransformation von dem ruhenden auf ein relativ zu diesem in gleichförmiger Translationsbewegung befindliches System.

Seien im „ruhenden“ Raume zwei Koordinatensysteme, d. h. zwei Systeme von je
drei von einem Punkte ausgehenden, aufeinander senkrechten starren materiellen
Linien, gegeben.  Die $X$-Achsen beider Systeme mögen zusammenfallen, ihre $Y$-
und $Z$-Achsen bezüglich parallel sein. Jedem Systeme sei ein starrer Maßstab
und eine Anzahl Uhren beigegeben, und es seien beide Maßstäbe sowie alle Uhren
beider Systeme einander genau gleich.

Es werde nun dem Anfangspunkte des einen der beiden Systeme ($k$) eine
(konstante) Geschwindigkeit v in Richtung der wachsenden $x$ des anderen,
ruhenden Systems ($K$) erteilt, welche sich auch den Koordinatenachsen, dem
betreffenden Maßstabe sowie den Uhren mitteilen möge. Jeder Zeit $t$ des ruhenden
Systems $K$ entspricht dann eine bestimmte Lage der Achsen des bewegten Systems
und wir sind aus Symmetriegründen befugt anzunehmen, daß die Bewegung von $k$ so
beschaffen sein kann, daß die Achsen des bewegten Systems zur Zeit $t$ (es ist
mit „$t$“ immer eine Zeit des ruhenden Systems bezeichnet) den Achsen des
ruhenden Systems parallel seien.

Wir denken uns nun den Raum sowohl vom ruhenden System $K$ aus mittels des
ruhenden Maßstabes als auch vom bewegten System $k$ mittels des mit ihm
bewegten Maßstabes ausgemessen und so die Koordinaten $x$, $y$, $z$ bez. $ξ$,
$η$, $ζ$, ermittelt. Es werde ferner mittels der im ruhenden System
befindlichen ruhenden Uhren durch Lichtsignale in der in § 1 angegebenen Weise
die Zeit $t$ des ruhenden Systems für alle Punkte des letzteren bestimmt, in
denen sich Uhren befinden; ebenso werde die Zeit $x$ des bewegten Systems für
alle Punkte des bewegten Systems, in welchen sich relativ zu letzterem ruhende
Uhren befinden, bestimmt durch Anwendung der in § 1 genannten Methode der
Lichtsignale zwischen den Punkten, in denen sich die letzteren Uhren befinden.

Zu jedem Wertsystem $x$, $y$, $z$, $t$, welches Ort und Zeit
eines Ereignisses im ruhenden System vollkommen bestimmt,
gehört ein jenes Ereignis relativ zum System $k$ festlegendes
Wertsystem $ξ$, $η$, $ζ$, $τ$, und es ist nun die Aufgabe zu lösen,
das diese Größen verknüpfende Gleichungssystem zu finden.

Zunächst ist klar, daß die Gleichungen _linear_ sein müssen wegen der
Homogenitätseigenschaften, welche wir Raum und Zeit beilegen.

Setzen wir $x'= x — v t$, so ist klar, daß einem im System $k$ ruhenden Punkte
ein bestimmtes, von der Zeit unabhängiges Wertsystem $x'$, $y$, $z$ zukommt.
Wir bestimmen zuerst $x$ als Funktion von $x'$, $y$, $z$ und $t$ Zu diesem
Zwecke haben wir in Gleichungen auszudrücken, daß $τ$ nichts anderes ist als der
Inbegriff der Angaben von im System $k$ ruhenden Uhren, welche nach der im § 1
gegebenen Regel synchron gemacht worden sind.

Vom Anfangspunkt des Systems $k$ aus werde ein Lichtstrahl zur Zeit $τ_0$ längs
der $X$-Achse nach $x'$ gesandt und von dort zur Zeit $τ_1$ nach dem
Koordinatenursprung reflektiert, wo er zur Zeit $τ_2$ anlange; so muß dann sein:
$ 1 / 2 (τ_0 + τ_2) = τ_1 $ 
oder, indem man die Argumente der Punktion $τ$ beifügt und das
Prinzip der Konstanz der Lichtgeschwindigkeit im ruhenden Systeme anwendet: 
$ 1/2 [ τ(0,0,0,t) + & τ(0,0,0,{t + frac(x', V-v) + frac(x', V+v)})] = \
                     & τ(x',0,0,t+frac(x', V-v)). $

Hieraus folgt, wenn man $x'$ unendlich klein wählt:

$ 1 / 2 ( frac(1, V-v) + frac(1, V+v) frac( diff τ, diff t ))  = frac( diff τ, diff x') + frac( 1, V-v) frac(diff τ, diff t), $
oder

$ frac(diff τ, diff x') + frac(v, V^2 - v^2) frac(diff τ, diff t) = 0. $

Es ist zu bemerken, daß wir statt des Koordinatenursprunges jeden anderen Punkt
als Ausgangspunkt des Lichtstrahles hätten wählen können und es gilt deshalb
die eben erhaltene Gleichung für alle Werte von $x'$, $y$, $z$.

Eine analoge Überlegung — auf die $H$ und $Z$-Achse angewandt — liefert, wenn man
beachtet, daß sich das Licht längs dieser Achsen vom ruhenden System aus
betrachtet stets mit der Geschwindigkeit $sqrt(V^2 - v^2)$. fortpflanzt:

$ frac( diff τ, diff y) = 0 $
$ frac( diff τ, diff z) = 0. $

Aus diesen Gleichungen folgt, da $tau$ eine lineare Funktion ist:

$ τ = a ( t - frac(v, V^2 - v^2) x' ) , $

wobei $a$ eine vorläufig unbekannte Funktion $phi(v)$ ist und der
Kürze halber angenommen ist, daß im Anfangspunkte von $k$
für $τ = 0$ $t = 0$ sei.

Mit Hilfe dieses Resultates ist es leicht, die Größen $ξ$, $η$, $ζ$ zu
ermitteln, indem man durch Gleichungen ausdrückt, daß sich das Licht (wie das
Prinzip der Konstanz der Lichtgeschwindigkeit in Verbindung mit dem
Relativitätsprinzip verlangt) auch im bewegten System gemessen mit der
Geschwindigkeit $V$ fortpflanzt. Für einen zur Zeit $τ = 0$ in Richtung der
wachsenden $ξ$ ausgesandten Lichtstrahl gilt:

Nun bewegt sich aber der Lichtstrahl relativ zum Anfangspunkt von $k$
im ruhenden System gemessen mit der Geschwindigkeit $V - v$, so daß gilt:

$ frac(x', V - v) = t. $

Setzen wir diesen Wert von $t$ in die Gleichung für $ξ$ ein, so
erhalten wir:

$ ξ = a frac(V^2, V^2 - v^2) x'. $

Auf analoge Weise finden wir durch Betrachtung von längs
den beiden anderen Achsen bewegte Lichtstrahlen:

$ η = V τ = a V ( t - frac(v, V^2 - v^2) x' ), $

wobei

$ frac(y, sqrt(V^2 - v^2)) = t; space.quad x' = 0 ; $

also

$ η = a frac(V, sqrt(V^2 - v^2)) y $

und

$ ζ = a frac(V, sqrt(V^2 - v^2)) z. $

Setzen wir für $x'$ seinen Wert ein, so erhalten wir:

$ τ &= φ(v)β(t- v/V^2 x), \

  ξ &= φ(v) β(x - v t ), \
  η &=  φ(v) y, \
  ζ &=  φ(v) z, $

wobei

$ β = frac(1, sqrt( 1 - (v/V)^2 ) ) $

und $φ$ eine vorläufig unbekannte Funktion von $v$ ist. Macht
man über die Anfangslage des bewegten Systems und über
den Nullpunkt von $τ$ keinerlei Voraussetzung, so ist auf den
rechten Seiten dieser Gleichungen je eine additive Konstante
zuzufügen.

Wir haben nun zu beweisen, daß jeder Lichtstrahl sich, im bewegten System
gemessen, mit der Geschwindigkeit. $V$ fortpflanzt, falls dies, wie wir
angenommen haben, im ruhenden System der Fall ist; denn wir haben den Beweis
dafür noch nicht geliefert, daß das Prinzip der Konstanz der
Lichtgeschwindigkeit mit dem Relativitätsprinzip vereinbar sei.

Zur Zeit $t = τ = 0$ werde von dem zu dieser Zeit gemeinsamen
Koordinatenursprung beider Systeme aus eine Kugelwelle ausgesandt, welche sich
im System $K$ mit der Geschwindigkeit $V$ ausbreitet. Ist $(x, y, z)$ ein eben
von dieser Welle ergriffener Punkt, so ist also

$ x^2 + y^2 + z^2 = V^2 t^2. $

Diese Gleichung transformieren wir mit Hilfe unserer Transformationsgleichungen
und erhalten nach einfacher Rechnung:

$ ξ^2 + η^2 + ζ^2 = V^2 τ^2. $

Die betrachtete Welle ist also auch im bewegten System betrachtet eine
Kugelwelle von der Ausbreitungsgeschwindigkeit $V$. Hiermit ist gezeigt, daß
unsere beiden Grundprinzipien miteinander vereinbar sind.

In den entwickelten Transformationsgleichungen tritt noch
eine unbekannte Funktion $φ$ von $v$ auf, welche wir nun bestimmen wollen.

Wir führen zu diesem Zwecke noch ein drittes Koordinatensystem $K'$ ein, welches
relativ zum System $h$ derart in Paralleltranslationsbewegung parallel zur
$Ξ$-Achse begriffen sei, daß sich dessen Koordinatenursprung mit der
Geschwindigkeit — $v$ auf der $Ξ$-Achse bewege. Zur Zeit $t=0$ mögen alle drei
Koordinatenanfangspunkte zusammenfallen und es sei für $t = x r= y = z = 0$ die
Zeit $t'$ des Systems $K'$ gleich Null. Wir nennen $x'$, $y'$, $z'$ die
Koordinaten, im System $K'$ gemessen, und erhalten durch zweimalige Anwendung
unserer Transformationsgleichungen:


$ t'& = φ(-v)β(-v) { τ + v/V^2 ξ }  &= φ(v)φ(-v)t, \
  x'& = φ(-v)β(-v) { ξ + v τ }    &= φ(v)φ(-v)x, \
  y'& = φ(-v)η                    &= φ(v)φ(-v)y, \
  z'& = φ(-v)ζ                    &= φ(v)φ(-v)z. $



Da die Beziehungen zwischen $x'$, $y'$, $z'$ und $x$, $y$, $z$ die Zeit $t$
nicht enthalten, so ruhen die Systeme $K$ und $K'$ gegeneinander, und es ist
klar, daß die Transformation von $K$ auf $K'$ die identische Transformation
sein muß. Es ist also:

$ φ(v) φ(-v) = 1. $

Wir fragen nun nach der Bedeutung von $φ(v)$. Wir fassen das Stück der
$W$-Achse des Systems $k$ ins Auge, das zwischen $ξ=0$, $η=0$, $ζ=0$ und $ξ=0$,
$η=l$, $ζ=0$ gelegen ist. Dieses Stück der $W$-Achse ist ein relativ zum System
$K$ mit der Geschwindigkeit $v$ senkrecht zu seiner Achse bewegter Stab, dessen
Enden in $K$ die Koordinaten besitzen:

Die Länge des Stabes, in $K$ gemessen, ist also $l$ / $φ(v)$; damit ist die
Bedeutung der Funktion $φ$ gegeben. Aus Symmetriegründen ist nun einleuchtend,
daß die im ruhenden System gemessene Länge eines bestimmten Stabes, welcher
senkrecht zu seiner Achse bewegt ist, nur von der Geschwindigkeit, nicht aber
von der Richtung und dem Sinne der Bewegung abhängig sein kann. Es ändert sich
also die im ruhenden System gemessene Länge des bewegten Stabes nicht, wenn $v$
mit $-v$ vertauscht wird. Hieraus folgt:

Aus dieser und der vorhin gefundenen Relation folgt, daß
$φ(v) = 1$ sein muß, so daß die gefundenen Transformationsgleichungen übergehen in:

$ τ &= β(t- v/V^2 x), \
  ξ &= β(x - v t), \
  η &= y, \
  ζ &= z, $

wobei

$ β = frac(1, sqrt( 1 - (v/V)^2 )), $

== § 4. Physikalische Bedeutung der erhaltenen Gleichungen, bewegte starre Körper und bewegte Uhren betreffend.

Wir betrachten eine starre Kugel #footnote[Das heißt einen Körper, welcher
ruhend untersucht Kugelgestalt besitzt.] vom Radius $R$, welche relativ zum
bewegten System $k$ ruht, und deren Mittelpunkt im Koordiuatenursprung von $h$
liegt. Die Gleichung der Oberfläche dieser relativ zum System $K$ mit der
Geschwindigkeit $v$ bewegten Kugel ist:

$ ξ^2 + η^2 + ζ^2 = R^2. $

Die Gleichung dieser Oberfläche ist in $x$, $y$, $z$ ausgedrückt zur
Zeit $t = 0$:

$ x^2 / ((sqrt(1 - (v/V)^2 ))^2) + y^2 + z^2 = R^2. $

Ein starrer Körper, welcher in ruhendem Zustande ausgemessen
die Gestalt einer Kugel hat, hat also in bewegtem Zustande —
vom ruhenden System aus betrachtet — die Gestalt eines
Rotationsellipsoides mit den Achsen

$ R sqrt(1 - (v/V)^2), R, R. $

Während also die $Y$- und $Z$-Dimension der Kugel (also
auch jedes starren Körpers von beliebiger Gestalt) 
durch die Bewegung nicht modifiziert erscheinen, erscheint die $X$-Dimension
im Verhältnis $1:sqrt(1-(v / V)^2)$ verkürzt, also um so stärker, je
größer $v$ ist, Für $v=V$ schrumpfen alle bewegten Objekte —
vom „ruhenden“ System aus betrachtet — in flächenhafte
Gebilde zusammen. Für Überlichtgeschwindigkeiten werden
unsere Überlegungen sinnlos; wir werden übrigens in den
folgenden Betrachtungen finden, daß die Lichtgeschwindigkeit
in unserer Theorie physikalisch die Bolle der unendlich großen
Geschwindigkeiten spielt.

Es ist klar, daß die gleichen Resultate von im „ruhenden“ System ruhenden
Körpern gelten, welche von einem gleichförmig bewegten System aus betrachtet
werden. — 

Wir denken uns ferner eine der Uhren, welche relativ zum ruhenden
System ruhend die Zeit $t$, relativ zum bewegten System ruhend die Zeit $τ$
anzugeben befähigt sind, im Koordinatenursprung von $k$ gelegen und so gerichtet,
daß sie die Zeit $τ$ angibt. Wie schnell geht diese Uhr, vom ruhenden System aus
betrachtet?

Zwischen die Größen $x$, $t$ und $τ$, welche sich auf den Ort
dieser Uhr beziehen, gelten offenbar die Gleichungen:

$ τ= 1 / sqrt(1-(v/V)^2) ( t - v/V^2 x) $

und

$ x=v t. $

Es ist also

$ τ=t sqrt(1 - (v/V)^2) = t - (1 - sqrt(1-(v/V)^2)) t, $

woraus folgt, daß die Angabe der Uhr (im ruhenden System
betrachtet) pro Sekunde um $(1 — sqrt(1 — (v/V)^2)$ Sek. oder — bis
auf Größen vierter und höherer Ordnung um $1/2 (v/V)^2$ Sek.
zurückbleibt.

Hieraus ergibt sich folgende eigentümliche Konsequenz.
Sind in den Punkten $A$ und $B$ von $K$ ruhende, im ruhenden
System betrachtet, synchron gehende Uhren vorhanden, und
bewegt man die Uhr in $A$ mit der Geschwindigkeit $v$ auf der
Verbindungslinie nach $B$, so gehen nach Ankunft dieser Uhr
in $B$ die beiden Uhren nicht mehr synchron, sondern die von $A$
nach $B$ bewegte Uhr geht gegenüber der von Anfang an in $B$
befindlichen um $1/2 t v^2 / V^2$ Sek. (bis auf Größen vierter und
höherer Ordnung) nach, wenn $t$ die Zeit ist, welche die Uhr
von $A$ nach $B$ braucht.

Man sieht sofort, daß dies Resultat auch dann noch gilt,,
wenn die Uhr in einer beliebigen polygonalen Linie sich von $A$
nach $B$ bewegt, und zwar auch dann, wenn die Punkte $A$
und $B$ zusammenfallen.

Nimmt man an, daß das für eine polygonale Linie bewiesene Resultat auch für
eine stetig gekrümmte Kurve gelte, so erhält man den Satz: Befinden sich in $A$
zwei synchron gehende Uhren und bewegt man die eine derselben auf einer
geschlossenen Kurve mit konstanter Geschwindigkeit, bis sie wieder nach $A$
zurückkommt, was $t$ Sek. dauern möge, so geht die letztere Uhr bei ihrer
Ankunft in $A$ gegenüber der unbewegt gebliebenen um $1/2 t (v/V)^2$ Sek. nach.
Man schließt daraus, daß eine am Erdäquator befindliche Unruhuhr um einen sehr
kleinen Betrag langsamer laufen muß als eine genau gleich beschaffene, sonst
gleichen Bedingungen unterworfene, an einem Erdpole befindliche Uhr.

== § 5. Additionstheorem der Geschwindigkeiten.

In dem längs der $X$-Achse des Systems $K$ mit der Geschwindigkeit $v$ bewegten
System $k$ bewege sich ein Punkt gemäß den Gleichungen:

$ ξ &= w_ξ τ, \
  η &= w_η τ, \
  ζ &= 0, $

wobei $w_ξ$  und $w_η$ Konstanten bedeuten.

Gesucht ist die Bewegung des Punktes relativ zum System $K$.
Führt man in die Bewegungsgleichungen des Punktes mit Hilfe
der in § 3 entwickelten Transformationsgleichungen die Größen
$x$, $y$, $z$, $t$ ein, so erhält man:

// TODO Align

$ x &= frac(w_ξ + v, 1 + v w_ξ/V^2) w_η t, $

$ y &= frac( sqrt( 1-(v/V)^2 ), 1+v w_ξ / V^2) w_η t, $

$ z &= 0. $

Das Gesetz vom Parallelogramm der Geschwindigkeiten gilt
also nach unserer Theorie nur in erster Annäherung. Wir
setzen:

$ U^2 = (frac(d x, d t))^2 + (frac(d y, d t))^2, $

und

$ α = arctan( w_y / w_x) ; $

$α$ ist dann als der Winkel zwischen den Geschwindigkeiten $v$
und $w$ anzusehen. Nach einfacher Rechnung ergibt sich:

$ U = frac( sqrt( (v^2 + w^2 + 2 v w cos(α) - (frac(v w sin(α), V)^2))),
1 + frac(v w cos(α), V^2)) $

Es ist bemerkenswert, daß $v$ und $w$ in symmetrischer Weise
in den Ausdruck für die resultierende Geschwindigkeit eingehen. 
Hat auch $w$ die Richtung der $X$-Achse ($Ξ$-Achse), so
erhalten wir:

Aus dieser Gleichung folgt, daß aus der Zusammensetzung
zweier Geschwindigkeiten, welche kleiner sind als $V$, stets eine
Geschwindigkeit kleiner als $V$ resultiert. Setzt man nämlich
$v=V—κ$, $w= V—λ$, wobei $κ$ und $λ$ positiv und kleiner als $V$
seien, so ist:

$ U= V frac(2 V - κ - λ, 2 V - κ - λ + frac( κ λ, V)) < V. $

Es folgt ferner, daß die Lichtgeschwindigkeit $V$ durch
Zusammensetzung mit einer „Unterlichtgeschwindigkeit“ nicht
geändert werden kann. Man erhält für diesen Fall:

$ U = frac( V + w , 1 + w/V ) = V. $

Wir hätten die Formel für $U$ für den Fall, daß $v$ und $m$ gleiche Richtung
besitzen, auch durch Zusammensetzen zweier Transformationen gemäß § 3 erhalten
können. Führen wir neben den in § 3 figurierenden Systemen $K$ und $k$ noch ein
drittes, zu $k$ in Parallelbewegung begriffenes Koordinatensystem $k'$ ein,
dessen Anfangspunkt sich auf der $Ξ$-Achse mit der Geschwindigkeit $w$ bewegt, so
erhalten wir zwischen den Größen $x$, $y$, $z$, $t$ und den entsprechenden Größen von
$k'$ Gleichungen, welche sich von den in § 3 gefundenen nur dadurch
unterscheiden, daß an Stelle von "v" die Größe

$ frac( v + w, 1 + frac( v w , V^2 ) ) $

tritt; man sieht daraus, daß solche Paralleltransformationen —
wie dies sein muß — eine Gruppe bilden,

Wir haben nun die für uns notwendigen Sätze der unseren
zwei Prinzipien entsprechenden Kinematik hergeleitet und gehen
dazu über, deren Anwendung in der Elektrodynamik zu zeigen.

= II. Elektrodynamischer Teil.

== § 6. Transformation der Maxwell-Hertzschen Gleichungen für den leeren Raum.  Über die Natur der bei Bewegung in einem Magnetfeld auftretenden elektromotorischen Kräfte.

Die Maxwell-Hertzschen Gleichungen für den leeren
Raum mögen gültig sein für das ruhende System $K$, so daß
gelten möge:

$ 1/V frac( ∂X, ∂t ) = frac( ∂N, ∂y ) - frac( ∂M, ∂z ), space.quad 
  1/V frac( ∂L, ∂t ) = frac( ∂Y, ∂z ) - frac( ∂Z, ∂y ), $
$ 1/V frac( ∂Y, ∂t ) = frac( ∂L, ∂z ) - frac( ∂N, ∂x ), space.quad 
  1/V frac( ∂M, ∂t ) = frac( ∂Z, ∂x ) - frac( ∂X, ∂z ), $
$ 1/V frac( ∂Z, ∂t ) = frac( ∂M, ∂x ) - frac( ∂L, ∂y ), space.quad 
  1/V frac( ∂N, ∂t ) = frac( ∂X, ∂y ) - frac( ∂Y, ∂x ), $

wobei $(X, Y, Z)$ den Vektor der elektrischen, $(L, M, N)$ den der
magnetischen Kraft bedeutet.

Wenden wir auf diese Gleichungen die in § 3 entwickelte Transformation an,
indem wir die elektromagnetischen Vorgänge auf das dort eingeführte, mit der
Geschwindigkeit $v$ bewegte Koordinatensystem beziehen, so erhalten wir die
Gleichungen:

$ 1/V frac( ∂X, ∂τ ) &= frac( ∂β(N - v/V Y), ∂η ) - frac( ∂β(M + v/V Z), ∂ζ), \
  1/V frac( ∂β(Y - v/V N), ∂τ ) &= frac( ∂L, ∂η ) - frac( ∂β(N + v/V Y), ∂ξ), \
  1/V frac( ∂β(Z + v/V M), ∂τ ) &= frac( ∂β(M + v/V Z), ∂ξ) - frac( ∂L, ∂η ), \
  1/V frac( ∂L, ∂τ ) &= frac( ∂β(Y + v/V N), ∂ζ) - frac( ∂β(Z + v/V M), ∂η), \
  1/V frac( ∂β(M + v/V Z), ∂τ) &= frac( ∂β(Z + v/V M), ∂ξ) - frac( ∂X, ∂ζ ), \
  1/V frac( ∂β(N + v/V Y), ∂τ) &= frac( ∂X, ∂ζ ) - frac( ∂β(Y - v/V N), ∂ξ)  $

wobei

$ β = 1 / sqrt( 1 - (v/V)^2 ). $

Das Relativitätsprinzip fordert nun, daß die MaxwellHertz sehen Gleichungen für
den leeren Raum auch im System $k$ gelten, wenn sie im System $K$ gelten, d. h.
daß für die im bewegten System $k$ durch ihre ponderomotorischen Wirkungen auf
elektrische bez. magnetische Massen definierten Vektoren der elektrischen und
magnetischen Kraft ($(X', Y', Z')$ und $(L',M',N')$) des bewegten Systems $k$
die Gleichungen gelten:

$ 1/V frac(∂X',∂τ) = frac(∂N',∂η)-frac(∂M',∂ζ), space.quad
  1/V frac(∂L',∂τ) = frac(∂Y',∂ζ)-frac(∂Z',∂η), $

$ 1/V frac(∂Y',∂τ) = frac(∂L',∂ζ)-frac(∂N',∂ξ), space.quad
  1/V frac(∂M',∂τ) = frac(∂Z',∂ξ)-frac(∂X',∂η), $

$ 1/V frac(∂Z',∂τ) = frac(∂M',∂ξ)-frac(∂L',∂η), space.quad
  1/V frac(∂N',∂τ) = frac(∂X',∂η)-frac(∂Y',∂ξ). $

Offenbar müssen nun die beiden für das System $k$ gefundenen Gleichungssysteme
genau dasselbe ausdrücken, da beide Gleichungssysteme den Maxwell-Hertzsehen
Gleichungen für das System $K$ äquivalent sind. Da die Gleichungen beider
Systeme ferner bis auf die die Vektoren darstellenden Symbole übereinstimmen,
so folgt, daß die in den Gleichungssystemen an entsprechenden Stellen
auftretenden Funktionen bis auf einen für alle Funktionen des einen
Gleichungssystems gemeinsamen, von $ξ$, $η$, $ζ$ und $τ$ unabhängigen,
eventuell von $v$ abhängigen Faktor $ψ(v)$ übereinstimmen müssen. Es gelten
also die Beziehungen:

$ X' = & ψ(v)X, & L' & =ψ(v)L, \
  Y' = & ψ(v) β(Y-v/V N), space.quad & M' & =ψ(v)β(M*v/V Z), \
  Z' = & ψ(v) β (Z+ v/V M), space.quad & N' & = ψ(v) β (N - v/V Y). $

Bildet man nun die Umkehrung dieses Gleichungssystems, erstens durch Auflösen
der soeben erhaltenen Gleichungen, zweitens durch Anwendung der Gleichungen auf
die inverse Transformation (von $k$ auf $K$), welche durch die Geschwindigkeit
— $v$ charakterisiert ist, so folgt, indem man berücksichtigt, daß die beiden
so erhaltenen Gleichungssysteme identisch sein müssen:

$ φ(v) dot.op φ(-v) = 1. $

Ferner folgt aus Symmetriegründen1) #footnote[Ist z. B. $X=Y=Z=L=M=0$ und $N eq.not 0$, so ist aus Symmetriegründen klar,
daß bei Zeichenwechsel von $-v$ ohne Änderung des numerischen Wertes auch $Y'$ sein
Vorzeichen ändern muß, ohne seinen numerischen Wert zu ändern.]

$ φ(v) = φ(-v); $

es ist also

$ φ(v) = 1, $

und unsere Gleichungen nehmen die Form an:

$ X' &= X,  space.quad  & L' &= L,\
  Y' &= β(Y - v/V N), space.quad &  M'  &= β(M+ v/V Z), \
  Z' &= β(Z + v/V M), space.quad &  N'  &= β(N-v/V Y). $

Zur Interpretation dieser Gleichungen bemerken wir folgendes, Es liegt eine
punktförmige Elektrizitätsmenge vor, welche im ruhenden System $K$ gemessen von
der Größe „eins“ sei, d. h  im ruhenden System ruhend auf eine gleiche
Elektrizitätsmenge im Abstand 1 cm die Kraft 1 Dyn ausübe. Nach dem
Relativitätsprinzip ist diese elektrische Masse auch im bewegten System
gemessen von der Größe "eins". Ruht diese Elektrizitätsmenge relativ zum
ruhenden System, so ist definitionsgemäß der Vektor $(X, Y, Z)$ gleich der auf
sie wirkenden Kraft, Ruht die Elektrizitätsmenge gegenüber dem bewegten System
(wenigstens in dem betreffenden Augenblick), so, ist die auf sie wirkende, in
dem bewegten System gemessene Kraft gleich dem Vektor $(X',Y',Z')$. Die ersten
drei der obigen Gleichungen lassen sich mithin auf folgende zwei Weisen in
Worte kleiden:

1. Ist ein punktförmiger elektrischer Einheitspol in einem elektromagnetischen
Felde bewegt, so wirkt auf ihn außer der elektrischen Kraft eine
„elektromotorische Kraft“, welche unter.  Vernachlässigung von mit der zweiten
und höheren Potenzen von $v/V$ multiplizierten Gliedern gleich ist dem mit der
Lichtgeschwindigkeit dividierten Vektorprodukt der Bewegungsgeschwindigkeit des
Einheitspoles und der magnetischen Kraft.  (Alte Ausdrucksweise.)

2. Ist ein punktförmiger elektrischer Einheitspol in einem elektromagnetischen
Felde bewegt, so ist die auf ihn wirkende Kraft gleich der an dem Orte des
Einheitspoles vorhandenen elektrischen Kraft, welche man durch Transformation
des Feldes auf ein relativ zum elektrischen Einheitspol ruhendes
Koordinatensystem erhält. (Neue Ausdrucksweise.)

Analoges gilt über die „magnetomotorischen Kräfte". Man
sieht, daß in der entwickelten Theorie die elektromotorische
Kraft nur die Rolle eines Hilfsbegriffes spielt, welcher seine
Einführung dem Umstande verdankt, daß die elektrischen und
magnetischen Kräfte keine von dem Bewegungszustande des
Koordinatensystems unabhängige Existenz besitzen.

Es ist ferner klar, daß die in der Einleitung angeführte
Asymmetrie bei der Betrachtung der durch Relativbewegung
eines Magneten und eines Leiters erzeugten Ströme verschwindet.
Auch werden die Fragen nach dem „Sitz“ der elektrodynamischen
elektromotorischen Kräfte (Unipolarmaschinen) gegenstandslos.

§ 7. Theorie des Doppelersehen Prinzips und der Aberration.

Im Systeme K befinde sich sehr ferne vom Koordinatenursprung eine Quelle
elektrodynamischer Wellen, welche in einem, den Koordinatenursprung
enthaltenden Raumteil mit genügender Annäherung durch die Gleichungen
dargestellt sei:

$ X &= X_0 sin( Φ ),& space.quad L & = L_0 sin( Φ ),& \
  Y &= Y_0 sin( Φ ),& space.quad M & = M_0 sin( Φ ),& space.quad Φ = ω ( t- frac( a x + b y + c z, V) ). \
  Z &= Z_0 sin( Φ ),& space.quad N & = N_0 sin( Φ ),& $ 

Hierbei sind $(X_0, Y_0, Z_0)$ und $(L_0, M_0, N_0)$ die Vektoren, welche die
Amplitude des Wellenzuges bestimmen, $a, b, c$ die Richtungskosinus der
Wellennormalen,

Wir fragen nach der Beschaffenheit dieser Wellen, wenn dieselben von einem in
dem bewegten System $k$ ruhenden Beobachter untersucht werden. — Durch Anwendung
der in § 6 gefundenen Transformationsgleichungen für die elektrischen und
magnetischen Kräfte und der in § 3 gefundenen Transformationsgleichungen für
die Koordinaten und die Zeit erhalten wir unmittelbar:

$ X' &= X_0 sin Φ', space.quad L' = L_0 sin ( Φ' ), \
  Y' &= β( Y_0 - v/V N_0 )  sin( Φ' ),& space.quad M' & = β(M_0 + v/V Z_0)  sin( Φ' ), \
  Z' &= β( Z_0 - v/V M_0 )  sin( Φ' ),& space.quad N' & = β(N_0 + v/V Y_0)  sin( Φ' ), $

$ Φ' = ω' ( τ - frac( a' ξ + b' η + c' ζ, V), $

wobei

$ ω' = ω β ( 1 - a v/V), $

$ a' = frac( a - v/V, 1 - a v/V ), $

$ b' = frac( b, β ( 1 - a v/V) ), $

$ c' = frac( c, β ( 1 - a v/V) ) $

gesetzt ist.

Aus der Gleichung für $ω'$ folgt: Ist ein Beobachter relativ zu einer unendlich
fernen Lichtquelle von der Frequenz $ν$ mit der Geschwindigkeit $v$ derart bewegt,
daß die Verbindungslinie „Lichtquelle-Beobachter“ mit der auf ein relativ zur
Lichtquelle ruhendes Koordinatensystem bezogenen Geschwindigkeit des
Beobachters den Winkel bildet, so ist die von dem Beobachter wahrgenommene
Frequenz $ν'$ des Lichtes durch die Gleichung gegeben:

$ ν' = ν frac( 1 - cos(φ) v/V , sqrt( 1 - (v/V)^2)) $

Dies ist das Doppelersche Prinzip für beliebige Geschwindigkeiten.  Für $φ = 0$
nimmt die Gleichung die übersichtliche Form an:

$ ν' = ν sqrt( frac( 1 - v/V, 1 + v/V ) ). $

Man sieht, daß — im Gegensatz zu der üblichen Auffassung —
für $v = — infinity$, $ν = infinity$ ist. 


Nennt man $φ’$ den Winkel zwischen Wellennormale (Strahlrichtung) im bewegten
System und der Verbindungslinie „Lichtquelle-Beobachter“, so nimmt die
Gleichung für $a'$ die Form an:

$ cos (φ') = frac( cos(φ) - v/V, 1 - v/V cos(φ)). $


Diese Gleichung drückt das Aberrationsgesetz in seiner allgemeinsten Form aus.
Ist $φ = π/2$, so nimmt die Gleichung die einfache Gestalt an:

$ cos φ' = - v/V . $

Wir haben nun noch die Amplitude der Wellen, wie dieselbe im bewegten System
erscheint, zu suchen. Nennt man $A$ bez. $A'$. die Amplitude der elektrischen oder
magnetischen Kraft im ruhenden bez. im bewegten System gemessen, so erhält man:

$ A'^2 = A^2 frac( (1 - v/V cos φ)^2, 1 - (v/V)^2 ), $

welche Gleichung für $φ = 0$ in die einfachere übergeht:

$ A'^2 = A^2 frac( 1 - v/V, 1 + v/V ) $

Es folgt aus den entwickelten Gleichungen, daß für einen Beobachter, der sich
mit der Geschwindigkeit $V$ einer Lichtquelle näherte, diese Lichtquelle
unendlich intensiv erscheinen müßte.

= § 8. Transformation der Energie der Lichtstrahlen. Theorie des auf vollkommene Spiegel ausgeübten Strahlungsdruckes.

Da $A^2/(8 π)$ gleich der Lichtenergie pro Volumeneinheit
ist, so haben wir nach dem Relativitätsprinzip $A'^2/(8 π)$ als die
Lichtenergie im bewegten System zu betrachten. Es wäre
daher $A'^2/A^2$ das Verhältnis der „bewegt gemessenen“ und
„ruhend gemessenen“ Energie eines bestimmten Lichtkomplexes,
wenn das Volumen eines Lichtkomplexes in $K$ gemessen und
in $k$ gemessen das gleiche wäre. Dies ist jedoch nicht der
Fall. Sind $a, b, c$ die Richtungskosinus der Wellennormalen
des Lichtes im ruhenden System, so wandert durch die Oberflächenelemente der mit Lichtgeschwindigkeit bewegten Kugel

$ ( x - V a t)^2 + (y - V b t)^2 + (z - V c t)^2 = R^2 $

keine Energie hindurch; wir können daher sagen, daß diese Fläche dauernd
denselben Lichtkomplex umschließt. Wir fragen nach der Energiemenge, welche
diese Fläche im System $k$ betrachtet umschließt, d. h. nach der Energie des
Lichtkomplexes relativ zum System $k$.

Die Kugelfläche ist — im bewegten System betrachtet — 
eine Ellipsoidfläche, welche zur Zeit $τ = 0$ die Gleichung besitzt:

$ (β ξ - α β v/V ξ)^2 + (η - b β v/V ξ)^2 + (ζ - c β v/V ξ)^2 = R^2 $

Nennt man $S$ das Volumen der Kugel, $S’$ dasjenige dieses
Ellipsoides, so ist, wie eine einfache Bechnung zeigt:

$ S'/S = frac( sqrt( 1 - (v/V)^2), 1 - v/V cos φ). $

Nennt man also $E$ die im ruhenden System gemessene, $E'$ die
im bewegten System gemessene Lichtenergie, welche von der
betrachteten Fläche umschlossen wird, so erhält man:
fläche

$ E'/E = frac( frac( A'^2, 8 π) S', frac(A^2, 8 π) S) =  frac( 1
- v/V cos φ, sqrt( 1 - (v/V)^2 ) ), $

welche Formel für $φ = 0$ in die einfachere übergeht:

$ E'/E = sqrt( frac( 1 - v/V, 1 + v/V ) ) . $


Es ist bemerkenswert, daß die Energie und die Frequenz
eines Lichtkomplexes sich nach demselben Gesetze mit dem
Bewegungszustande des Beobachters ändern.

Es sei nun die Koordinatenebene $ξ = 0$ eine vollkommen spiegelnde Fläche, an
welcher die im letzten Paragraph betrachteten ebenen Wellen reflektiert werden.
Wir fragen nach dem auf die spiegelnde Fläche ausgeübten Lichtdruck und nach
der Richtung, Frequenz und Intensität des Lichtes nach der Reflexion.

Das einfallende Licht sei durch die Größen $A$, $cos φ$, $ν$
(auf das System $K$ bezogen) definiert. Von $k$ aus betrachtet
sind die entsprechenden Größen:

$ A' = A frac( 1 - v/V cos φ, sqrt( 1 - (v/V)^2) ), $

$ cos φ' = frac( cos φ - v/V, 1 - v/V cos φ ), $

$ ν' = ν frac( 1 - v/V cos φ,  sqrt( 1 - (v/V)^2) ). $

Für das reflektierte Licht erhalten wir, wenn wir den Vorgang auf das System k beziehen:

$ A'' & = A' \
  cos φ'' & = - cos φ', \
  ν'' & = ν'. $ 


Endlich erhält man durch Rücktransformieren aufs ruhende
System K für das reflektierte Licht:

$ A''' = A'' frac( 1 + v/V cos φ'', sqrt( 1 - (v/V)^2)) = A frac(
1- 2 v/V cos φ + (v/V)^2, 1 - (v/V)^2), $ 

$ cos φ''' = frac( cos φ'' + v/V, 1 + v/V cos φ'') = - frac( ( 1
+ (v/V)^2) cos φ - 2 v/V, 1 - 2 v/V cos φ + (v/V)^2), $

$ ν''' = ν'' frac( 1 + v/V cos φ'', sqrt( 1 - (v/V)^2 )) = ν
frac( 1 - 2 v/V cos φ + (v/V)^2, ( 1 - v/V )^2 ) $

Die auf die Flächeneinheit des Spiegels pro Zeiteinheit auftreffende (im
ruhenden System gemessene) Energie ist offenbar $A^2/(8 π) (P cos φ — v)$. Die
von der Flächeneinheit des Spiegels in der Zeiteinheit sich entfernende Energie
ist $A'''^3/(8 π)(-V cos φ''' + v)$. Die Differenz dieser beiden Ausdrücke ist
nach dem Energieprinzip die vom Lichtdrucke in der Zeiteinheit geleistete
Arbeit. Setzt man die letztere gleich dem Produkt $P.v$, wobei $P$ der
Lichtdruck ist, so erhält man:

$ P = 2 frac( A^2, 8 π) frac( (cos φ - v/V )^2, 1 - (v/V)^2). $

In erster Annäherung erhält man in Übereinstimmung mit der
Erfahrung und mit anderen Theorien

$ P = 2 frac( A^2, 8 π) cos^2 φ . $

Nach der hier benutzten Methode können alle Probleme der Optik bewegter Körper
gelöst werden. Das Wesentliche ist, daß die elektrische und magnetische Kraft
des Lichtes, welches durch einen bewegten Körper beeinflußt wird, auf ein
relativ zu dem Körper ruhendes Koordinatensystem transformiert werden. Dadurch
wird jedes Problem der Optik bewegter Körper auf eine Reihe von Problemen der
Optik ruhender Körper zurückgeführt.

= § 9. Transformation der Maxwell-Hertzsehen Gleichungen mit Berücksichtigung der Konvektionsströme.

Wir gehen aus von den Gleichungen:

$ 1/V { u_x ρ + frac(∂X,∂t)} = frac(∂N,∂y) - frac(∂M,∂z), space.quad 1/V frac(∂L,∂t) = frac(∂Y,∂z) - frac(∂Z,∂y), $

$ 1/V { u_y ρ + frac(∂Y,∂t)} = frac(∂L,∂z) - frac(∂N,∂x), space.quad 1/V frac(∂M,∂t) = frac(∂Z,∂x) - frac(∂X,∂z), $

$ 1/V { u_z ρ + frac(∂Z,∂t)} = frac(∂M,∂x) - frac(∂L,∂y), space.quad 1/V frac(∂N,∂t) = frac(∂X,∂y) - frac(∂Y,∂x), $

wobei

$ ρ = frac(∂X,∂x) + frac(∂Y,∂y) + frac(∂Z,∂z) $

die $4 π$-facbe Dichte der Elektrizität und $(u_x, u_y, u_z)$ den Geschwindigkeitsvektor der Elektrizität bedeutet. Denkt man
sich die elektrischen Massen unveränderlich an kleine, starre
Körper (Ionen, Elektronen) gebunden, so sind diese Gleichungen
die elektromagnetische Grundlage der Lorentzschen Elektrodynamik und Optik bewegter Körper.

Transformiert man diese Gleichungen, welche im System $K$
gelten mögen, mit Hilfe der Transformationsgleichungen von
§ 3 und § 6 auf das System $k$, so erhält man die Gleichungen:

$ 1/V { u_ξ ρ' + frac(∂X',∂τ)} = frac(∂N',∂η) - frac(∂M',∂ζ),
space.quad frac(∂L',∂τ) = frac(∂Y',∂ζ) - frac(∂Z',∂η), $

$ 1/V { u_η ρ' + frac(∂Y',∂τ)} = frac(∂L',∂ζ) - frac(∂N',∂ξ),
space.quad frac(∂M',∂τ) = frac(∂Z',∂ξ) - frac(∂X',∂ζ), $

$ 1/V { u_ζ ρ' + frac(∂Z',∂τ)} = frac(∂M',∂ξ) - frac(∂L',∂η),
space.quad frac(∂N',∂τ) = frac(∂X',∂η) - frac(∂Y',∂ξ), $

wobei

$ frac( u_x - v, 1 - frac( u_x v, V^2 ) ) &= u_ξ,& \
 frac( u_y, β( 1 - frac( u_x v, V^2 ) ) ) &= u_η,& space.quad ρ' = frac(
∂X', ∂ξ) + frac(∂Y', ∂η) + frac(∂Z', ∂ζ) = β( 1 - frac(v u_x,
V^2) ) ρ .\
 frac( u_z, β( 1 - frac( u_x v, V^2) ) ) &= u_ζ . & $

Da — wie aus dem Additionstheorem der Geschwindigkeiten (§ 5) folgt — der
Vektor $(u_ξ, u_η, u_ζ)$ nichts anderes ist als die Geschwindigkeit der
elektrischen Massen im System $k$ gemessen, so ist damit gezeigt, daß unter
Zugrundelegung unserer kinematischen Prinzipien die elektrodynamische Grundlage
der Lorentzschen Theorie der Elektrodynamik bewegter Körper dem
Relativitätsprinzip entspricht.

Es möge noch kurz bemerkt werden, daß aus den entwickelten Gleichungen leicht
der folgende wichtige Satz gefolgert werden kann: Bewegt sich ein elektrisch
geladener Körper beliebig im Raume und ändert sich hierbei seine Ladung nicht,
von einem mit dem Körper bewegten Koordinatensystem aus betrachtet, so bleibt
seine Ladung auch — von dem „ruhenden“ System $K$ aus betrachtet — konstant.

= § 10. Dynamik des (langsam beschleunigten) Elektrons.

In einem elektromagnetischen Felde bewege sich ein punktförmiges, mit einer
elektrischen Ladung $s$ versehenes Teilchen (im folgenden „Elektron“ genannt),
über dessen Bewegungsgesetz wir nur folgendes annehmen;

Ruht das Elektron in einer bestimmten Epoche, so erfolgt
in dem nächsten Zeitteilchen die Bewegung des Elektrons nach
den Gleichungen

$ μ frac( d^2 x, d thin t^2) = ε X \
  μ frac( d^2 y, d thin t^2) = ε Y \
  μ frac( d^2 z, d thin t^2) = ε Z, $

wobei $x, y, z$ die Koordinaten des Elektrons, $μ$ die Masse
des Elektrons bedeutet, sofern dasselbe langsam bewegt ist.

Es besitze nun zweitens das Elektron in einer gewissen Zeitepoche die
Geschwindigkeit $v$. Wir suchen das Gesetz, nach welchem sich das Elektron im
unmittelbar darauf folgenden Zeitteilchen bewegt.  Ohne die Allgemeinheit der
Betrachtung zu beeinflussen, können und wollen wir annehmen, daß das Elektron
in dem Momente, wo wir es ins Auge fassen, sich im Koordinatensprung befinde
und sich längs der $X$-Achse des Systems $K$ mit der Geschwindigkeit $v$
bewege. Es ist dann einleuchtend, daß das Elektron im genannten Momente $(t =
0)$ relativ zu einem längs der $X$-Achse mit der konstanten Geschwindigkeit $v$
parallelbewegten Koordinatensystem $k$ ruht.

Aus der oben gemachten Voraussetzung in Verbindung
mit dem Relativitätsprinzip ist klar, daß sich das Elektron in
der unmittelbar folgenden Zeit (für kleine Werte von t) vom
System k aus betrachtet nach den Gleichungen bewegt:

$ μ frac( d^2 ξ, d thin τ^2) = ε X' \
  μ frac( d^2 η, d thin τ^2) = ε Y' \
  μ frac( d^2 ζ, d thin τ^2) = ε Z', $


wobei die Zeichen $ξ, η, ζ, τ, X', Y', Z’$ sieh auf das System $k$ beziehen.
Setzen wir noch fest, daß für $t = x = y = z = 0$ $τ =ξ = η = ζ = 0$ sein soll,
so gelten die Transformationsgleichungen der §§ 8 und 6, so daß gilt:


$ τ &= β ( t - v/V^2 x ), \
  ξ &= β ( x - v t), #h(5em) & X' &= X, \
  η &= y , #h(5em) & Y' &= β(Y - v/V N), \
  ζ &= z , #h(5em) & Z' &= β(Z + v/V M). $

Mit Hilfe dieser Gleichungen transformieren wir die obigen
Bewegungsgleichungen vom System h auf das System K und
erhalten:

$
  (A) space.quad cases( frac(d^2x, d thin t^2) = ε/μ 1/β^3 X\,  ,
       frac(d^2y, d thin t^2) = ε/μ 1/β ( Y - v/V N )\, ,
       frac(d^2z, d thin t^2) = ε/μ 1/β ( Z - v/V M ).)
$

Wir fragen nun in Anlehnung an die übliche Betrachtungsweise nach der
„longitudinalen“ und „transversalen“ Masse des bewegten Elektrons. Wir
schreiben die Gleichungen (A) in der Form

$ μ β^3 frac(d^2 x, d thin t^2) &= ε X = ε X', \
  μ β^2 frac(d^2 y, d thin t^2) &= ε β ( Y - v/V N) = ε Y', \
  μ β^2 frac(d^2 z, d thin t^2) &= ε β ( Z + v/V M) = ε Z'. $

und bemerken zunächst, daß $ε X', ε Y', ε Z'$ die Komponenten
der auf das Elektron wirkenden ponderomotorischen Kraft sind,
und zwar in einem in diesem Moment mit dem Elektron mit
gleicher Geschwindigkeit wie dieses bewegten System betrachtet.
(Diese Kraft könnte beispielsweise mit einer im letzten System
ruhenden Federwage gemessen werden.) Wenn wir nun diese
Kraft schlechtweg „die auf das Elektron wirkende Kraft"
nennen und die Gleichung

$
 "Massenanzahl" times "Beschleunigungszahl" = "Kraftzahl"
$ 

aufrechterhalten, und wenn wir ferner festsetzen, daß die Beschleunigungen im
ruhenden System $K$ gemessen werden sollen, so erhalten wir aus obigen
Gleichungen:

$
 "Longitudinale Masse" &= frac( μ, (sqrt( 1 - (v/V)^2 ))^3), \
 "Transversale Masse" &= frac( μ, 1 - (v/V)^2 ).
$

Natürlich würde man bei anderer Definition der Kraft und der Beschleunigung
andere Zahlen für die Massen erhalten; man ersieht daraus, daß man bei der
Vergleichung verschiedener Theorien der Bewegung des Elektrons sehr vorsichtig
verfahren muß.

Wir bemerken, daß diese Resultate über die Masse auch für die ponderabeln
materiellen Punkte gilt; denn ein ponderabler materieller Punkt kann durch
Zufügen einer beliebig Kleinen elektrischen Ladung zu einem Elektron (in
unserem Sinne) gemacht werden.

Wir bestimmen die kinetische Energie des Elektrons.  Bewegt sich ein Elektron
vom Koordinatenursprung des Systems $K$ aus mit der Anfangsgeschwindigkeit 0
beständig auf der $X$-Achse unter der Wirkung einer elektrostatischen Kraft
$X$, so ist klar, daß die dem elektrostatischen Felde entzogene Energie den
Wert $integral ε X d x$ hat. Da das Elektron langsam beschleunigt sein soll und
infolgedessen keine Energie in Form von Strahlung abgeben möge, so muß die dem
elektrostatischen Felde entzogene Energie gleich der Bewegungsenergie $W$ des
Elektrons gesetzt werden. Man erhält daher, indem man beachtet, daß während des
ganzen betrachteten Bewegungsvorganges die erste der Gleichungen (A) gilt:

$
  W = integral ε X d x = integral_0^v β^3 v d v = μ V^2 { frac( 1, sqrt( 1 - (v/V)^2 )) - 1 }.
$

$W$ wird also für $v = V$ unendlich groß. Überlichtgeschwindigkeiten haben —
wie bei unseren früheren Resultaten — keine Existenzmöglichkeit.

Auch dieser Ausdruck für die kinetische Energie muß dem oben angeführten
Argument zufolge ebenso für ponderable Massen gelten.

Wir wollen nun die aus dem Gleichungssystem (A) resultierenden, dem Experimente
zugänglichen Eigenschaften der Bewegung des Elektrons aufzählen.

1. Aus der zweiten Gleichung des Systems (A) folgt, daß eine elektrische Kraft
$Y$ und eine magnetische Kraft $N$ dann gleich stark ablenkend wirken auf ein
mit der Geschwindigkeit $v$ bewegtes Elektron, wenn $Y=N.v/V$. Man ersieht
also, daß die Ermittelung der Geschwindigkeit des Elektrons aus dem Verhältnis
der magnetischen Ablenkbarkeit $A_m$ und der elektrischen Ablenkbarkeit $A_e$
nach unserer Theorie für beliebige Geschwindigkeiten möglich ist durch
Anwendung des Gesetzes:

$
  frac(A_m, A_e) = v/V.
$

Diese Beziehung ist der Prüfung durch das Experiment zugänglich, da die
Geschwindigkeit des Elektrons auch direkt, z. B. mittels rasch oszillierender
elektrischer und magnetischer Felder, gemessen werden kann.

2. Aus der Ableitung für die kinetische Energie des Elektrons folgt, daß
zwischen der durchlaufenen Potentialdifferenz und der erlangten Geschwindigkeit
$v$ des Elektrons die Beziehung gelten muß:

$
  P = integral X d x = μ/ε V^2 { frac( 1, sqrt( 1 - (v/V)^2 )) - 1 }.
$

3. Wir berechnen den Krümmungsradius $R$ der Bahn,
wenn eine senkrecht zur Geschwindigkeit des Elektrons wirkende
magnetische Kraft N (als einzige ablenkende Kraft) vorhanden
ist. Aus der zweiten der Gleichungen (A) erhalten wir:

$
  - frac( d^2 y, d thin t^2 ) = v^2/R = ε/μ v/V N dot.op sqrt( 1-
    (v/V)^2)
$

oder

$
  R = V^2 μ/ε dot.op frac( v/V , sqrt( 1 - (v/V)^2 ) ) dot.op 1/N .
$

Diese drei Beziehungen sind ein vollständiger Ausdruck
für die Gesetze, nach denen sich gemäß vorliegender Theorie
das Elektron bewegen muß.

Zum Schlusse bemerke ich, daß mir beim Arbeiten an
dem hier behandelten Probleme mein Freund und Kollege
M. Besso treu zur Seite stand und daß ich demselben manche
wertvolle Anregung verdanke.

Bern, Juni 1905.

#set text(
  size: 8pt
)
(Eingegangen 30. Juni 1905.)

